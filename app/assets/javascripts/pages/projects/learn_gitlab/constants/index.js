import { s__ } from '~/locale';

export const ACTION_TEXT = {
  gitWrite: s__('LearnGitLab|Create a repository'),
  userAdded: s__('LearnGitLab|Invite your colleagues'),
  pipelineCreated: s__('LearnGitLab|Set-up CI/CD'),
  trialStarted: s__('LearnGitLab|Start a free trial of GitLab Gold'),
  codeOwnersEnabled: s__('LearnGitLab|Add code owners'),
  requiredMrApprovalsEnabled: s__('LearnGitLab|Enable require merge approvals'),
  mergeRequestCreated: s__('LearnGitLab|Submit a merge request (MR)'),
  securityScanEnabled: s__('LearnGitLab|Run a Security scan using CI/CD'),
};
